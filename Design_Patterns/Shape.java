package Design_Patterns;

public interface Shape {
	   void draw();
	}
