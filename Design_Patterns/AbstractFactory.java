package Design_Patterns;

public abstract class AbstractFactory {
	   abstract Shape getShape(String shapeType) ;
	}